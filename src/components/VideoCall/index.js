import React, {useState, useEffect, useRef} from 'react';
import {StyleSheet, TouchableOpacity, View, Dimensions} from 'react-native';
import {mediaDevices, RTCView} from 'react-native-webrtc';
import {SOCKET} from '../../config';
import Peer from 'react-native-peerjs';
import Draggable from 'react-native-draggable';
import * as keys from '../Constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Avatar, Button, Text} from 'react-native-elements';

export default function index({navigation, route}) {
  const [openMyCam, setOpenMyCam] = useState(true);
  const videoGrid1 = useRef();
  const {action, room, toWho, avatarTo, userID, caller} = route.params;
  const videoGrid2 = useRef();
  const [stream, setStream] = useState(null);
  const [streamOthers, setStreamOthers] = useState(null);
  const [isAnswer, setIsAnswer] = useState(false);
  const [widthButton, setWidthButton] = useState(0);
  const [coordinate, setCoordinate] = useState({
    x: 0,
    y: 0,
  });
  const peer = new Peer(undefined);
  const [userId, setUserId] = useState(userID);
  const [coordinateStreamView, setCoordinateStreamView] = useState({
    x: 0,
    y: 0,
  });

  useEffect(() => {
    console.log(Dimensions.get('screen'));
    console.log(Dimensions.get('window'));
  }, []);
  useEffect(() => {
    AsyncStorage.getItem(keys.User_Name).then((userName) => {
      if (userName) {
        peer.on('open', (id) => {
          if (room && action && userName.length > 0) {
            SOCKET.emit('join-room', room, id, userName);
            setUserId(id);
          }
        });
      }
    });
  }, [action, room]);
  // useEffect(() => {
  //   if (action === 'answer') setOpenMyCam(false);
  // }, []);
  // useEffect(() => {
  //   console.log(
  //     'stream',
  //     stream,
  //     '\nstreamOthers',
  //     streamOthers,
  //     '\nwidthButton',
  //     widthButton,
  //     '\nopenMyCam',
  //     openMyCam,
  //     '\nuserId',
  //     userId,
  //   );
  // }, [stream, streamOthers, widthButton, openMyCam, userId]);
  useEffect(() => {
    SOCKET.on('redirect-chat-page', () => {
      navigation.goBack();
    });
    return () => {
      SOCKET.emit('user-close-video-chat');
    };
  }, []);
  const cancelCall = () => {
    SOCKET.emit('user-close-video-chat');
    navigation.goBack();
  };

  useEffect(() => {
    mediaDevices
      .getUserMedia({
        audio: true,
        video: true,
      })
      .then(async (streamFunction) => {
        // Got stream!
        const userName = await AsyncStorage.getItem(keys.User_Name);
        if (action === 'call') {
          SOCKET.emit('call-user', {
            uri: `/chat/video-call?room=${room}&action=answer`,
            room: room,
            name: userName,
          });
        }
        setStream(streamFunction);
      })
      .catch((error) => {
        // Log error
        console.log(error);
      });
  }, [action, room]);
  useEffect(() => {
    SOCKET.on('user-connected', (data) => {
      if (action === 'call') {
        call(data);
      }
    });
    if (action === 'answer') {
      answer();
    }
  }, [action]);
  const call = (userId) => {
    mediaDevices
      .getUserMedia({
        audio: true,
        video: true,
      })
      .then((stream) => {
        if (userId) {
          var call = peer.call(userId, stream);
          // const video = document.createElement('video');
          call.on('stream', function (remoteStream) {
            setStream(stream);
            setStreamOthers(remoteStream);
            setOpenMyCam(false);
            // addVideoStream(video, remoteStream, 'guestCam');
          });
        }
      })
      .catch(function (err) {
        console.log('Failed to get local stream', err);
      });
  };

  // console.log(isAnswer);
  const answer = () => {
    //console.log(1);

    //const getUserMedia = mediaDevices.getUserMedia;
    peer.on('call', function (call) {
      mediaDevices
        .getUserMedia({
          video: true,
          audio: true,
        })
        .then(function (stream) {
          //console.log(stream);
          setStream(stream);
          call.on('stream', function (remoteStream) {
            //cam của ng ta
            setStreamOthers(remoteStream);
            // setStream(stream);
            setOpenMyCam(false);
            setIsAnswer(true);
          });
          call.answer(stream);
        })
        .catch(function (err) {
          console.log('Failed to get local stream', err);
        });
    });
  };
  useEffect(() => {
    navigation.setOptions({
      title: `Video call with ${toWho ? toWho : ''}${caller ? caller : ''}`,
    });
  }, []);
  return (
    <View
      style={{
        flex: 1,
        position: 'relative',
      }}>
      {!streamOthers ? (
        action === 'call' ? (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              marginTop: 15,
            }}>
            <Avatar rounded size="xlarge" source={{uri: avatarTo}} />
            <Text h4>{toWho}</Text>
            <Text h4 h4Style={{fontWeight: '500'}}>
              calling ...
            </Text>
          </View>
        ) : (
          <></>
        )
      ) : (
        <RTCView
          ref={videoGrid1}
          streamURL={streamOthers.toURL()}
          style={{
            flex: 1,
          }}
        />
      )}
      {!openMyCam && (
        <Draggable
          x={coordinate.x} //{Dimensions.get('window').width - widthButton} //
          y={coordinate.y} //{Dimensions.get('window').height - 100}
          minX={0}
          minY={0}
          maxX={Dimensions.get('window').width}
          maxY={Dimensions.get('window').height + 36}
          renderColor="black"
          isCircle
          // onDragRelease={(e, f, c) => setCoordinate({x: c.left, y: c.top})}
          onDrag={() => {}}
          onShortPressRelease={() => setOpenMyCam(true)}>
          <Feather name="video" style={{padding: 5}} color="white" size={25} />
        </Draggable>
      )}
      {openMyCam && (
        <View
          onLayout={({nativeEvent: {layout}}) => {
            setCoordinate({
              x: layout.x + layout.width - 36,
              y: layout.y + layout.height - 36, //36 là layout w,h của btn hide camera,
            });
          }}
          style={{
            width: 250,
            height: 300,
            position: 'absolute',
            backgroundColor: 'black',
            bottom: 40,
            right: 0,
          }}>
          {stream && (
            <RTCView
              style={{flex: 1}}
              ref={videoGrid2}
              streamURL={stream.toURL()}
            />
          )}
          <TouchableOpacity
            style={{
              position: 'absolute',
              top: 0,
            }}
            onLayout={(e) => {
              setWidthButton(e.nativeEvent.layout.width);
            }}
            onPress={() => setOpenMyCam(false)}>
            <Feather
              name="video-off"
              style={{padding: 5}}
              color="white"
              size={25}
            />
          </TouchableOpacity>
        </View>
      )}
      <Button
        buttonStyle={{
          backgroundColor: 'red',
          width: 50,
          height: 50,
          borderRadius: 25,
        }}
        containerStyle={{
          position: 'absolute',
          bottom: 5,
          zIndex: 999,
          alignSelf: 'center',
        }}
        icon={<MaterialIcons name="call-end" size={25} color="white" />}
        onPress={() => cancelCall()}
      />
    </View>
  );
}

const styles = StyleSheet.create({});
