import MainContent from './MainContent';
import React, {useRef, useState, useEffect} from 'react';
import {
  View,
  TextInput,
  ScrollView,
  Image,
  Pressable,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import FontAwesome5nIcon from 'react-native-vector-icons/FontAwesome5';
import FontAwesomenIcon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import API from '../API/API';
import * as styles from './Styles';
import {Button, Text} from 'react-native-elements';
import * as keys from '../Constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Modal from 'react-native-modal';
import {SOCKET} from '../../config';
import moment from 'moment';
import {CreateNotification} from '../../services/user';
import {useDispatch} from 'react-redux';
import {navigate_To_Other} from '../Redux/Actions/OtherProfile.Action';
import {Appbar} from 'react-native-paper';

export default function ViewLCS(props) {
  const [listLike, setListLike] = useState(props.likeList);
  const [likeNumber, setLikeNumber] = useState(props.likeList.length);
  const scrollRef = useRef(null);
  const textComment = useRef(null);
  const [visible, setVisible] = useState(false);
  const [liked, setLiked] = useState(props.liked);
  const [loading, setLoading] = useState(false);
  const [inputCmt, setInputCmt] = useState('');
  const [listComment, setListComment] = useState(props.listComment);
  const [smallCmt, setSmallCmt] = useState([]);
  const device = Dimensions.get('window');
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [layoutModal, setLayoutModal] = useState(device);
  const [scrollOffset, setScrollOffset] = useState(0);
  const [likeDetail, setLikeDetail] = useState(false);

  useEffect(() => {
    if (props.listComment) {
      setListComment(props.listComment);
      setSmallCmt(listComment.slice(listComment.length - 2));
    }
    if (props.likeList) {
      setListLike(props.likeList);
      setLikeNumber(props.likeList.length);
    }
    if (props.liked === true) setLiked(true);
    if (props.liked === false) setLiked(false);
  }, [props]);

  useEffect(() => {
    const joinRoomComment = async () => {
      if (visible) {
        const name = await AsyncStorage.getItem(keys.User_ProfLink);
        const room = props.index;
        if (room) {
          SOCKET.emit('join', {name, room}, (err) => {
            if (err) alert(err);
          });
        }
      }
    };
    joinRoomComment();
  }, [props.index, visible]);

  useEffect(() => {
    SOCKET.on('comment', async (comment) => {
      if (comment.room === props.index) {
        listComment.push({
          id: comment.id,
          comment: comment.comment,
          linkProfile: comment.linkProfile,
          user_avatar: comment.avatar,
          user_name: comment.userName,
        });
        setSmallCmt(listComment.slice(listComment.length - 2));
        if (textComment.current) textComment.current.clear();
        setInputCmt('');
        if (scrollRef.current) {
          setTimeout(() => {
            scrollRef.current.scrollToEnd();
          }, 1 * 100);
        }
      }
    });
  }, []);

  //for testing
  const makeid = (length) => {
    var result = [];
    var characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result.push(
        characters.charAt(Math.floor(Math.random() * charactersLength)),
      );
    }
    return result.join('');
  };

  const Like = async () => {
    const token = await AsyncStorage.getItem(keys.User_Token);
    const username = await AsyncStorage.getItem(keys.User_Name);
    const userAvatar = await AsyncStorage.getItem(keys.User_Avatar);
    setLoading(true);
    if (!liked) {
      const profileLink = await AsyncStorage.getItem(keys.User_ProfLink);
      // SOCKET.emit(
      //   'subscribe-friend-chanel',
      //   {name: profileLink, room: '1234567890'},
      //   (err) => {
      //     if (err) {
      //       console.log('error noti: ', err);
      //     }
      //   },
      // );

      const route = 'status/update-status';
      const param = {
        status_id: props.index,
        like: parseInt(likeNumber) + 1,
      };
      const header = {
        Authorization: 'bearer' + token,
      };
      const api = new API();
      setTimeout(() => {
        api
          .onCallAPI('post', route, {}, param, header)
          .then((res) => {
            if (res.data.error_code !== 0) {
              alert(res.data.message);
            } else {
              setListLike(res.data.data);
              setLiked(true);
              setLoading(false);
              setLikeNumber(likeNumber + 1);

              //push notification
              let notificationData = {
                current_user_name: profileLink,
                status_id: props.index,
                owner_id: props.userID, //owner of status
                content: `${username} liked your status`,
                current_user_avatar: userAvatar,
                moment: moment().fromNow(),
              };
              SOCKET.emit('client-liked-status', notificationData);
            }
          })
          .catch((err) => {
            alert(err);
          });
      }, 1 * 1000);

      setTimeout(async () => {
        let params = {
          owner: props.userID,
          type: 'status-like',
          item: props.index,
        };

        const notiResponse = await CreateNotification(params);
      }, 1000);
    } else {
      const route = 'status/update-status';
      const param = {
        status_id: props.index,
        like: parseInt(likeNumber) - 1,
        is_unlike: 1,
      };
      const header = {
        Authorization: 'bearer' + token,
      };
      const api = new API();
      setTimeout(() => {
        api
          .onCallAPI('post', route, {}, param, header)
          .then((res) => {
            if (res.data.error_code !== 0) {
              alert(res.data.message);
            } else {
              setListLike(res.data.data);
              setLiked(false);
              setLoading(false);
              setLikeNumber(likeNumber - 1);
            }
          })
          .catch((err) => {
            alert(err);
          });
      }, 2 * 1000);
    }
  };
  const commentButton = async () => {
    if (inputCmt.trim() !== '') {
      const profileLink = await AsyncStorage.getItem(keys.User_ProfLink);
      const userAvatar = await AsyncStorage.getItem(keys.User_Avatar);
      const username = await AsyncStorage.getItem(keys.User_Name);
      const param = {
        status_id: props.index,
        content: inputCmt,
      };
      const link = 'comment';
      const token = await AsyncStorage.getItem(keys.User_Token);
      const header = {
        Authorization: 'bearer' + token,
      };
      // console.log(link, param, header);
      // return;
      const api = new API();
      await api
        .onCallAPI('post', link, {}, param, header)
        .then((res) => {
          if (res.data.error_code !== 0) {
            alert(res.data.message);
          } else {
            setTimeout(async () => {
              let params = {
                owner: props.userID,
                type: 'status-comment',
                item: props.index,
              };

              const {status, message} = await CreateNotification(params);
              if (!status) {
                //  console.log('errors when create notification: ', message);
                alert('Errors when create notification. Please try again!');
              } else {
                SOCKET.emit(
                  'sendComment',
                  inputCmt,
                  username,
                  profileLink,
                  userAvatar,
                  props.index,
                  () => {
                    setInputCmt('');
                  },
                );

                //push notification
                let notificationData = {
                  current_user_name: profileLink,
                  status_id: props.index,
                  owner_id: props.userID, //owner of status
                  content: `${username} commented on your status`,
                  current_user_avatar: userAvatar,
                  moment: moment().fromNow(),
                };
                SOCKET.emit('client-liked-status', notificationData);
              }
            }, 200);
          }
        })
        .catch((err) => {
          // console.log('error: ', err);
          alert(err);
        });
    }
  };

  const inputComment = (type) => {
    // type = 1: ngoài modal tức là input comment tượng trưng dùng để mở modal
    // type = 2: trong modal tức là input comment dùng để comment thật
    // if (type === 1)
    return (
      // <ScrollView keyboardShouldPersistTaps="handled" style={{flex: 1}}>
      <View style={styles.stylesViewLCS.inputcmtContainer}>
        <TouchableOpacity
          onPress={(e) => {
            if (type !== 1) {
              setVisible(true);
            } else e.preventDefault();
          }}
          style={{
            flex: 1,
            maxHeight: 150,
          }}>
          <TextInput
            onFocus={() => scrollRef.current?.scrollToEnd()}
            editable={type === 1 ? true : false}
            multiline
            autoFocus
            ref={textComment}
            onChangeText={(e) => setInputCmt(e)}
            placeholder="Write comment"
            style={[styles.stylesViewLCS.textCmt]}
            value={inputCmt}
            // numberOfLines={5}
          />
        </TouchableOpacity>
        <Button
          buttonStyle={{backgroundColor: 'transparent'}}
          style={styles.stylesViewLCS.btnComment}
          onPress={commentButton}
          icon={
            <FontAwesomenIcon
              name="send"
              style={{width: 26}}
              size={25}
              color="rgba(0,0,0,.8)"
            />
          }
        />
      </View>
      // </ScrollView>
    );
  };
  const listLikeDetails = (like, index) => {
    const user = like.user_id;
    like.user = user;
    return (
      <View
        key={index}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          padding: 10,
        }}>
        <TouchableOpacity onPress={() => navigationByComment(like)}>
          <Image
            source={{uri: like.avatar}}
            style={styles.stylesViewLCS.avatarComment}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigationByComment(like)}>
          <Text style={styles.stylesViewLCS.commentUserName}>
            {like.user_name}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
  const navigationByComment = (comment) => {
    dispatch(navigate_To_Other(comment.user));
    navigation.navigate('OtherUser', {
      userId: comment.user,
    });
    setVisible(false);
  };

  const smallLike = () => (
    // list like thể hiện với dạng danh sách rút gọn
    <View
      onLayout={(e) => setLayoutModal(e.nativeEvent.layout)}
      style={{
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
      }}>
      <View style={styles.stylesViewLCS.containerContent}>
        {listLike.length > 0 && (
          <View
            style={{
              padding: 10,
              flexDirection: 'row',
              position: 'relative',
              borderBottomWidth: 1,
              backgroundColor: 'rgba(0,0,0,.1)',
            }}>
            <Pressable
              onPress={() => {
                setLikeDetail(true);
              }}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                flex: 10,
              }}>
              <View style={styles.stylesViewLCS.viewIcon}>
                <AntDesignIcon name="like1" size={15} color="white" />
              </View>
              <Text style={styles.stylesViewLCS.likeNumber}>
                {whoLike(listLike)}
              </Text>
            </Pressable>
            <Button
              buttonStyle={{
                backgroundColor: 'transparent',
              }}
              containerStyle={{borderRadius: 100}}
              icon={
                liked ? (
                  <AntDesignIcon name="like1" size={20} color="blue" />
                ) : (
                  <AntDesignIcon
                    name="like2"
                    size={20}
                    color="rgba(0,0,0,.6)"
                  />
                )
              }
              loading={loading}
              loadingProps={{animating: true, color: '#999'}}
              loadingStyle={{borderColor: 'black'}}
              onPress={() => Like()}
            />
          </View>
        )}
        <View style={{flex: 10}}>
          {listComment.length > 0 ? (
            <ScrollView
              ref={scrollRef}
              overScrollMode="always"
              scrollToOverflowEnabled
              contentContainerStyle={{justifyContent: 'center'}}
              // showsHorizontalScrollIndicator={false}
              snapToInterval={450}
              decelerationRate="fast"
              onScroll={handleOnScroll}
              style={{marginTop: 10}}>
              {listComment.map(SingleComment)}
            </ScrollView>
          ) : (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <FontAwesome5nIcon
                name="comments"
                color="rgba(0,0,0,.5)"
                size={50}
              />
              <Text h4>No Comment Yet</Text>
              <Text h4 h4Style={{fontWeight: '900'}}>
                Be the first to comment
              </Text>
            </View>
          )}
        </View>
        {inputComment(1)}
      </View>
    </View>
  );
  const SingleComment = (comment, i) => {
    return (
      <Pressable
        onPress={() => {
          setVisible(true);
          // setCmtVisible(true);
          setLikeDetail(false);
        }}
        key={i}
        style={styles.stylesViewLCS.SingleCommentContainer}>
        <TouchableOpacity onPress={() => navigationByComment(comment)}>
          <Image
            source={{uri: comment.user_avatar}}
            style={styles.stylesViewLCS.avatarComment}
          />
        </TouchableOpacity>
        <View style={styles.stylesViewLCS.commentBox}>
          <TouchableOpacity onPress={() => navigationByComment(comment)}>
            <Text style={styles.stylesViewLCS.commentUserName}>
              {comment.user_name}
            </Text>
          </TouchableOpacity>
          <Text style={styles.stylesViewLCS.captionText}>
            {comment.comment}
          </Text>
        </View>
      </Pressable>
    );
  };
  const whoLike = (list) => {
    var i = 0;
    var listWho = '';
    const test =
      'Đãbảođặttênngắnthôi MàsaoNócứ Dàinhưthếnày, Đãbảođặttênngắnthôi MàsaoNócứ Dàinhưthếnày';

    for (i = 0; i < list.length; i++) {
      listWho += list[i].user_name + ', ';
    }
    if (list.length > 2) {
      listWho =
        list[0].user_name +
        ', ' +
        list[1].user_name +
        ' và ' +
        (list.length - 2) +
        ' người khác';
    }
    if (list.length <= 2)
      listWho = listWho.substring(0, listWho.lastIndexOf(','));
    return listWho;
  };

  const handleOnScroll = (event) => {
    if (event.nativeEvent.contentOffset.y)
      setScrollOffset(event.nativeEvent.contentOffset.y);
  };
  const handleScrollTo = (p) => {
    if (scrollRef.current) {
      scrollRef.current.scrollTo(p);
      Keyboard.dismiss();
    }
  };

  return (
    <>
      <Modal
        isVisible={visible}
        hasBackdrop={false}
        swipeDirection={['up', 'down']}
        animationIn="slideInUp"
        scrollTo={handleScrollTo}
        onBackdropPress={() => {
          setVisible(false);
          Keyboard.dismiss();
        }}
        scrollOffset={scrollOffset}
        scrollOffsetMax={layoutModal.height}
        onSwipeCancel={() => {
          setVisible(true);
        }}
        onSwipeComplete={() => {
          setVisible(false);
          Keyboard.dismiss();
        }}
        onBackButtonPress={() => {
          setVisible(false);
          Keyboard.dismiss();
        }}
        animationOut="slideOutDown"
        propagateSwipe={true}
        animationInTiming={600}
        animationOutTiming={600}
        avoidKeyboard
        hideModalContentWhileAnimating
        style={{margin: 0}}>
        {!likeDetail ? (
          smallLike()
        ) : (
          <View
            onLayout={(e) => setLayoutModal(e.nativeEvent.layout)}
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'white',
            }}>
            <Appbar.Header style={{backgroundColor: '#fff'}}>
              <Appbar.BackAction onPress={() => setLikeDetail(false)} />
              <Appbar.Content title="People who like this status" />
            </Appbar.Header>
            {listLike.map(listLikeDetails)}
          </View>
        )}
      </Modal>

      {listLike.length > 0 && (
        <Pressable
          onPress={() => {
            setVisible(true);
            setLikeDetail(true);
          }}
          style={[
            styles.stylesViewLCS.iconLike,
            {marginTop: 10, marginLeft: 10},
          ]}>
          <View style={styles.stylesViewLCS.viewIcon}>
            <AntDesignIcon name="like1" size={12} color="white" />
          </View>
          <Text style={styles.stylesViewLCS.likeNumber}>{listLike.length}</Text>
        </Pressable>
      )}
      <View style={styles.stylesViewLCS.viewBtn}>
        <Button
          buttonStyle={{backgroundColor: 'transparent'}}
          containerStyle={{margin: 5, flex: 1}}
          icon={
            liked ? (
              <AntDesignIcon name="like1" size={20} color="blue" />
            ) : (
              <AntDesignIcon name="like2" size={20} color="rgba(0,0,0,.6)" />
            )
          }
          loading={loading}
          loadingProps={{animating: true, color: '#999'}}
          loadingStyle={{borderColor: 'black'}}
          onPress={() => Like()}
          title={'Like'}
          titleStyle={[
            {
              marginHorizontal: 5,
              fontSize: 16,
            },
            liked ? {color: 'blue'} : {color: 'rgba(0,0,0,.6)'},
          ]}
        />
        <Button
          buttonStyle={{backgroundColor: 'transparent'}}
          containerStyle={{margin: 5, flex: 1}}
          linearGradientProps={null}
          icon={
            <FontAwesome5nIcon
              name="comment-alt"
              size={20}
              color="rgba(0,0,0,.8)"
            />
          }
          loadingProps={{animating: true}}
          title="Comments"
          onPress={() => {
            setVisible(true);
            // history.back();
          }}
          titleStyle={{
            marginHorizontal: 5,
            color: 'rgba(0,0,0,.6)',
            fontSize: 16,
          }}
        />
      </View>
      {/* <View style={{}}>{smallCmt.map(SingleComment)}</View> */}
      {inputComment(2)}
    </>
  );
}
const styless = StyleSheet.create({
  container: {
    width: '100%',
    height: 92,
  },
  row: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    width: '100%',
    paddingTop: 0,
    paddingBottom: 0,
    paddingRight: 11,
    paddingLeft: 11,
    alignItems: 'center',
  },
  input: {
    height: 50,
    flex: 1,
    paddingTop: -20,
    paddingBottom: -20,
    paddingLeft: 20,
    paddingRight: 8,
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,.5)',
    borderRadius: 30,
    height: 35,
    justifyContent: 'space-around',
  },
});
