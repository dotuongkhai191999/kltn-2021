import 'react-native-gesture-handler';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  TouchableOpacity,
  LogBox,
} from 'react-native';
import {Button, Text} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import AppBar from '../AppBar';
import {SafeAreaView} from 'react-navigation';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Profile from '../Profile/';
import Notifications from '../Notifications';
import Messengers from '../Messengers';
import OtherProfile from '../OtherProfile';
import {SOCKET} from '../../config';
import {showMessage} from 'react-native-flash-message';
import {Avatar} from 'react-native-paper';
import {Clear_List_Chat, Get_Group_Chat} from '../Redux/Actions/Chat.Action';
import {createStackNavigator} from '@react-navigation/stack';
import DetailMessenger from '../Messengers/DetailMessenger';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Settings from '../Settings';
import {
  Get_IntroUser,
  Get_StatusProfile,
} from '../Redux/Actions/ProfileUser.Action';
import DrawerContent from './DrawerContent';
import {useHistory} from 'react-router';
import UpStatusScreen from '../ToolBar/UpStatusScreen';
import {clear_Home, ReloadHome} from '../Redux/Actions/Home.Action';
import {Fetch_Notification} from '../Redux/Actions/Notification.Action';
import {useNavigation} from '@react-navigation/core';
import StatusScreen from './Status.Screen';
import * as myConst from '../Constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getUserRooms} from '../../services/user';
import ConfirmImageScreen from '../Profile/ConfirmImage.Screen';
import VideoCall from '../VideoCall';
import Modal from 'react-native-modal';
import jwt from 'jwt-decode';

LogBox.ignoreLogs(['Reanimated 2']);
LogBox.ignoreAllLogs();
const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const Home = () => {
  // const OtherProfileState = useSelector((state) => state.OtherProfile);

  const HomeTabs = () => {
    const [tabColor, setTabColor] = useState('#65676B');
    const [activeHomeTab, setActiveHomeTab] = useState(true);
    const [activeNotiTab, setActiveNotiTab] = useState(false);
    const [activeProfileTab, setActiveProfileTab] = useState(false);
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const storeState = useSelector((state) => state.HomePage);
    useEffect(() => {
      SOCKET.emit('user-access-web');

      AsyncStorage.getItem(myConst.User_Token).then(async (value) => {
        if (value) {
          let userNoSign = null;
          await AsyncStorage.getItem(myConst.User_ProfLink).then((val) => {
            if (val) {
              userNoSign = val;
            }
          });

          if (userNoSign) {
            const {data, status, message} = await getUserRooms();
            if (status) {
              if (data?.length > 0) {
                data.map((item) => {
                  let userAccessData = {
                    name: userNoSign,
                    room: item,
                  };

                  SOCKET.emit('user-access', userAccessData, (err) => {
                    if (err) {
                      alert('Internal Server Errors! Please try later.');
                      // console.log('user access errors: ', err);
                    }
                  });
                  return;
                });
              }
            } else {
              alert('Internal Server Errors! Please try later.');
              // console.log('error: ', message);
            }
          }
        }
      });
    }, []);

    const notificationTitle = (data) => {
      return (
        <TouchableOpacity
          onPress={() => {
            if (data.type && data.type === 'message') {
              navigation.navigate('DetailMessages', {
                avatar: data.avatarOther,
                friend_chat: data.friend_chat,
                chat_group_id: data.chat_group_id,
              });
            } else {
              navigation.navigate('statusScreen', {statusId: data.StatusId});
            }
          }}
          style={{
            width: '100%',
            height: '100%',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View style={{justifyContent: 'space-around', flex: 1}}>
            <Avatar.Image
              size={35}
              style={{borderWidth: 0.5}}
              source={{uri: data.avatar}}
            />
          </View>
          <View
            style={{
              justifyContent: 'space-around',
              flex: 1,
              marginHorizontal: 5,
            }}>
            <Text>{data.content}</Text>
            <Text>{data.moment}</Text>
          </View>
        </TouchableOpacity>
      );
    };
    const history = useHistory();

    SOCKET.on('server-popup-notification', (data) => {
      if (data.type && data.type === 'message') {
        showMessage({
          message: notificationTitle({
            avatar: data.current_user_avatar,
            content: data.content,
            moment: data.moment,
            friend_chat: data.friend_chat,
            chat_group_id: data.chat_group_id,
            avatarOther: data.avatar,
            type: 'message',
          }),
          type: 'default',
          color: 'black',
        });
      } else {
        showMessage({
          message: notificationTitle({
            avatar: data.current_user_avatar,
            content: data.content,
            moment: data.moment,
            StatusId: data.status_id,
          }),
          type: 'default',
          color: 'black',
        });
      }
    });
    return (
      <Tab.Navigator
        activeColor="#1877F2"
        initialRouteName="Home"
        inactiveColor="#65676B"
        barStyle={{backgroundColor: '#ffff'}}
        shifting={true}>
        <Tab.Screen
          name="Home"
          listeners={({navigation, route}) => ({
            tabPress: (e) => {
              // dispatch(clear_Home());
              dispatch(ReloadHome());
            },
          })}
          options={{
            tabBarIcon: () => {
              return (
                <MaterialCommunityIcons name="home" color="#1877F2" size={26} />
              );
            },
          }}
          // component={Settings}
          component={AppBar}
        />
        <Tab.Screen
          name="Notifications"
          listeners={({navigation, route}) => ({
            tabPress: (e) => {
              dispatch(Fetch_Notification());
            },
          })}
          options={{
            tabBarIcon: () => {
              return <Entypo name="bell" color="#1877F2" size={26} />;
            },
          }}
          component={Notifications}
        />
        <Tab.Screen
          name="Messengers"
          listeners={({navigation, route}) => ({
            tabPress: (e) => {
              // e.preventDefault();
              dispatch(Clear_List_Chat());
              dispatch(Get_Group_Chat());
              // navigation.navigate('Messengers', {
              //   screen: 'SmallMessengers',
              //   resetTime: true,
              // });
            },
          })}
          keyboardHidesTabBar={true}
          options={{
            tabBarIcon: () => {
              return (
                <MaterialCommunityIcons
                  name="facebook-messenger"
                  color="#1877F2"
                  size={26}
                />
              );
            },
          }}
          component={Messengers}
        />
        <Tab.Screen
          name="Profile"
          listeners={({navigation, route}) => ({
            tabPress: (e) => {
              dispatch(Get_StatusProfile());
              dispatch(Get_IntroUser());
            },
          })}
          options={{
            tabBarIcon: () => (
              <Ionicons
                name="person-circle-outline"
                color="#1877F2"
                size={26}
              />
            ),
          }}
          component={Profile}
        />
      </Tab.Navigator>
    );
  };

  const StackNavigator = () => {
    const [videoCallUri, setVideoCallUri] = useState('');
    const [room, setRoom] = useState();
    const [modalCall, setModalCall] = useState(false);
    const [caller, setCaller] = useState('');

    useEffect(() => {
      SOCKET.emit('user-access-web');
      let timeOut;
      SOCKET.on('call-user', (data) => {
        setModalCall(true);
        setRoom(data.room);
        setVideoCallUri(data.uri);
        // timeOut = setTimeout(() => {
        //   cancelCall();
        // }, 15000);
        setCaller(data.name);
      });
      return () => {
        // clearTimeout(timeOut);
      };
    }, []);

    const cancelCall = () => {
      setModalCall(false);
      SOCKET.emit('user-close-video-chat');
    };
    const navigation = useNavigation();
    const answerCall = () => {
      setModalCall(false);
      navigation.navigate('VideoCall', {
        action: 'answer',
        room: room,
        caller: caller,
      });
    };
    return (
      <>
        <Modal
          isVisible={modalCall}
          backdropColor="#000"
          // swipeDirection={['up', 'down']}
          backdropOpacity={0.5}
          hasBackdrop
          animationInTiming={600}
          // onBackdropPress={() => cancelCall()}
          style={{margin: 0}}
          animationOutTiming={600}>
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'center',
              padding: 15,
            }}>
            <Text h4 h4Style={{textAlign: 'center'}}>
              {caller} <Text>is calling</Text>
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Button
                buttonStyle={{backgroundColor: 'red'}}
                containerStyle={{margin: 5, flex: 1}}
                icon={<MaterialIcons name="call-end" size={15} color="white" />}
                onPress={() => cancelCall()}
              />
              <Button
                buttonStyle={{backgroundColor: 'green'}}
                containerStyle={{margin: 5, flex: 1}}
                icon={<MaterialIcons name="call" size={15} color="white" />}
                onPress={() => answerCall()}
              />
            </View>
          </View>
        </Modal>

        <Stack.Navigator initialRouteName="VideoCall">
          <Stack.Screen
            name="Home"
            options={{headerShown: false}}
            // component={Settings}
            component={HomeTabs}
          />

          <Stack.Screen
            name="OtherUser"
            options={{
              headerShown: false,
              // cái chỗ thanh tìm kiếm khi ở screen của người khác
              //  thì viết vào header screen ở trong other Profile, custom lại cái header
            }}
            component={OtherProfile}
          />
          <Stack.Screen
            name="DetailMessages"
            component={DetailMessenger}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Settings"
            component={Settings}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="subToolBar"
            component={UpStatusScreen}
            options={{
              headerShown: false,
            }}
            initialParams={{isPhotoPress: false}}
          />
          <Stack.Screen
            name="statusScreen"
            component={StatusScreen}
            options={{
              title: false,
            }}
          />
          <Stack.Screen
            name="ProfileImage"
            component={ConfirmImageScreen}
            options={{
              title: false,
            }}
          />
        </Stack.Navigator>
      </>
    );
  };
  const [information, setInformation] = useState();
  useEffect(async () => {
    const token = await AsyncStorage.getItem(myConst.User_Token);
    setInformation(jwt(token));
  }, []);
  const DrawerNavigator = () => (
    <Drawer.Navigator
      drawerContentOptions={{drawerLockMode: 'locked-closed'}}
      drawerContent={(props) => <DrawerContent {...props} />}>
      <Drawer.Screen name="test2" component={StackNavigator} />
    </Drawer.Navigator>
  );
  return (
    <>
      <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <Stack.Navigator initialRouteName="Drawer">
          <Stack.Screen
            name="Drawer"
            options={{headerShown: false}}
            initialParams={{userID: information?.user_id}}
            component={DrawerNavigator}
          />
          <Stack.Screen
            name="VideoCall"
            initialParams={{userID: information?.user_id}}
            // initialParams={{
            //   action: 'call',
            //   room: '60e1ce09c67b00003c006554',
            //   toWho: 'foo user',
            //   avatarTo:
            //     'https://kltn-api.herokuapp.com/api/image?file=default.jpg',
            // }}
            // options={{title: ''}}
            component={VideoCall}
          />
        </Stack.Navigator>
      </SafeAreaView>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    fontSize: 20,
  },
  nav: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  navItem: {
    flex: 1,
    alignItems: 'center',
    padding: 10,
  },
  subNavItem: {
    padding: 5,
  },
  topic: {
    textAlign: 'center',
    fontSize: 15,
  },
});
export default Home;
