import React, { useEffect, useLayoutEffect, useState } from 'react';
import { StyleSheet, ScrollView, Alert, RefreshControl } from 'react-native';
import ContentStatus from '../ContentStatus';
import API from '../API/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as storeKeys from '../Constants';

export default function StatusScreen({ navigation, route }) {
  const [srcData, setSrcData] = useState({});
  const { statusId } = route.params;
  useLayoutEffect(() => {
    fetchStatus();
  }, []);
  const fetchStatus = async () => {
    const val = await AsyncStorage.getItem(storeKeys.User_Token);
    const param = {
      status_id: statusId,
    };
    const route = 'status/get-status';

    const header = {
      Authorization: 'bearer' + val,
    };
    const api = new API();
    api.onCallAPI('get', route, {}, param, header).then((res) => {
      if (res.data.error_code !== 0) {
        alert(res.data.message);
        setError(true);
      } else {
        if (!res.data.data.comments) res.data.data.comments = [];
        setSrcData(res.data.data);
      }
    });
  };
  const [refreshing, setRefreshing] = useState();
  const [error, setError] = useState(false);
  const onRefresh = () => {
    setRefreshing(true);
    setSrcData({});
  };
  useEffect(() => {
    if (Object.keys(srcData).length !== 0) setRefreshing(false);
    else {
      fetchStatus();
    }
  }, [srcData]);
  useEffect(() => {
    if (error) {
      Alert.alert(
        'Warning',
        'There is error, when you connect to our server. Please check your internet connection, and try it again later',
        [
          {
            text: 'OK',
            onPress: () => navigation.goBack(),
            style: 'cancel',
          },
        ],
        {
          cancelable: true,
          onDismiss: () => navigation.goBack(),
        },
      );
    }
  }, [error]);
  if (Object.keys(srcData).length !== 0) {
    return (
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        keyboardShouldPersistTaps="handled"
        style={{ backgroundColor: 'white', marginTop: 10 }}>
        <ContentStatus profilePage={false} srcData={srcData} />
      </ScrollView>
    );
  }
  return <></>;
}

const styles = StyleSheet.create({});
