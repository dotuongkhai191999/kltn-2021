import React, {useState, useEffect} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import Avatar from '../Avatar';
import {Text} from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as keys from '../Constants';
import {styles} from './style';
import {useNavigation} from '@react-navigation/core';
export default function ToolBar() {
  const navigation = useNavigation();
  const [enablePost, setEnablePost] = useState(false);
  const [userAvatar, setUserAvatar] = useState();
  useEffect(() => {
    const getInfoOwner = async () => {
      const avatar = await AsyncStorage.getItem(keys.User_Avatar);
      setUserAvatar(avatar);
    };
    getInfoOwner();
  }, []);

  const popUpStatusModal = () => {
    navigation.navigate('subToolBar', {
      isPhotoPress: false,
    });
  };
  const photoPress = () => {
    navigation.navigate('subToolBar', {
      isPhotoPress: true,
    });
  };

  return (
    <>
      <View style={styles.container}>
        <View style={styles.row}>
          <View style={{marginRight: 10}}>
            <Avatar isHomePage={true} source={userAvatar} />
          </View>
          <TouchableOpacity
            style={styles.input}
            onPress={() => popUpStatusModal()}>
            <Text>What's on your mind?</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity
            // onPress={() => pressLiveCamera()}
            style={[styles.menu, {borderEndWidth: 0.2}]}>
            <Ionicons name="ios-videocam" size={22} color="#f44337"></Ionicons>
            <Text style={{padding: 5, fontWeight: '900'}}>Live</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => photoPress()}
            style={styles.menu}>
            <MaterialIcons
              name="photo-size-select-actual"
              size={20}
              color="#4caf50"></MaterialIcons>
            <Text style={{padding: 5, fontWeight: '900'}}>Photo</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}
