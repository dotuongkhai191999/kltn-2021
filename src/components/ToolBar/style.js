import {StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 92,
  },
  row: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    width: '100%',
    paddingTop: 0,
    paddingBottom: 0,
    paddingRight: 11,
    paddingLeft: 11,
    alignItems: 'center',
  },
  input: {
    height: 50,
    flex: 1,
    paddingTop: -20,
    paddingBottom: -20,
    paddingLeft: 20,
    paddingRight: 8,
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,.5)',
    borderRadius: 30,
    height: 35,
    justifyContent: 'space-around',
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: '#f0f0f0',
    marginTop: 10,
  },
  menu: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 42,
    zIndex: 9999,
  },
  memuButton: {
    paddingLeft: 11,
    fontWeight: '500',
    fontSize: 12,
    backgroundColor: '#ffffff',
  },
  separator: {
    width: 1,
    height: 26,
    backgroundColor: '#f0f0f0',
  },
  popupStatus: {
    // flex: 1,/
    position: 'relative',
  },
  popupStatusHeader: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 'auto',
    // marginTop: "auto",
    position: 'relative',
  },
  popupStatusContent: {
    flex: 6,
    marginTop: 'auto',
  },
  popupStatusFooter: {
    flex: 1,
    backgroundColor: 'red',
  },
  popupStatusHeaderBack: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  popupStatusHeaderContent: {
    flex: 6,
  },
  popupStatusHeaderButtonSubmit: {
    flex: 2.5,
  },
  popupStatusUser: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 4,
    marginTop: 1,
  },
  popupStatusMainContent: {
    flex: 4,
    marginTop: 5,
  },
  submitButton: {
    width: 85,
    height: 35,
    top: 2,
    borderRadius: 10,
    // backgroundColor: status === '' ? '#EEEEEE': '#1058B0',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 16,
  },
  statusButton: {
    // width: 70,
    // height:30,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#dddddd',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 0,
  },
  avatarBlock: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  statusUserRestBlock: {
    flex: 5,
  },
  statusUserName: {
    flex: 1,
  },
  statusOption: {
    flex: 1,
  },
});
export const stylesUpSttScreen = StyleSheet.create({
  popupStatus: {
    // flex: 1,/
    position: 'relative',
  },
  popupStatusHeader: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 'auto',
    // marginTop: "auto",
    position: 'relative',
  },
  popupStatusHeaderBack: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  popupStatusHeaderContent: {
    flex: 6,
  },
  popupStatusHeaderButtonSubmit: {
    flex: 2.5,
  },
  submitButton: {
    width: 85,
    height: 35,
    top: 2,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 16,
  },
  divider: {
    width: '100%',
    height: 1,
    backgroundColor: '#f0f0f0',
    marginTop: 10,
  },
  popupStatusContent: {
    flex: 6,
    marginTop: 'auto',
  },
  popupStatusUser: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 4,
    marginTop: 1,
  },
  avatarBlock: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  statusUserRestBlock: {
    flex: 5,
  },
  statusUserName: {
    flex: 1,
  },
  statusOption: {
    flex: 1,
  },
  statusButton: {
    // width: 70,
    // height:30,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#dddddd',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 0,
  },
  popupStatusMainContent: {
    flex: 4,
    marginTop: 5,
  },
});
