import React, {useRef, useState, useEffect} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Avatar from '../Avatar';
import {Text} from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import DropDownPicker from 'react-native-dropdown-picker';
import * as StatusServices from '../../services/status';
import {clear_Home, ReloadHome} from '../Redux/Actions/Home.Action';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as keys from '../Constants';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ImageCropPicker from 'react-native-image-crop-picker';
import {useDispatch, useSelector} from 'react-redux';
import {stylesUpSttScreen} from './style';
import ImageGrid from '../ContentStatus/ImageGrid';

export default function UpStatusScreen({navigation, route}) {
  const [status, setStatus] = useState('');
  const [option, setOption] = useState('pub');
  const [userInfo, setUserInfo] = useState({});
  const styles = stylesUpSttScreen;
  const inputTextRef = useRef(null);
  const [files, setFiles] = useState([]);
  // const gridRef = useRef();
  const [imageGrid, setImageGrid] = useState([]);
  const ProfilePage = useSelector((state) => state.ProfileInfo);
  const dispatch = useDispatch();
  const [isDisabled, setisDisabled] = useState(true);
  useEffect(() => {
    if (route.params.isPhotoPress) {
      library();
    } else return;
  }, []);
  useEffect(() => {
    if (status.length > 0 || files.length > 0) {
      setisDisabled(false);
    } else setisDisabled(true);
  }, [status, files]);

  useEffect(() => {
    const getInfoOwner = async () => {
      const avatar = await AsyncStorage.getItem(keys.User_Avatar);
      const userName = await AsyncStorage.getItem(keys.User_Name);
      setUserInfo({
        avatar: avatar,
        userName: userName,
      });
    };
    getInfoOwner();
  }, []);

  const postStatus = async () => {
    setisDisabled(true);
    if (status.length > 0 || files.length > 0) {
      const params = {
        caption: status,
        status_setting: option,
        type: 2,
        option: 2,
      };
      let upStatusResponse;
      const formdata = new FormData();
      if (files.length !== 0) {
        for (var i = 0; i < files.length; i++) {
          const today = new Date();
          const str = files[i].path;
          var n = str.lastIndexOf('/');
          var res = str.substr(n + 1);
          const file = {
            uri: files[i].path,
            name: res,
            type: files[i].mime,
          };
          formdata.append('file[]', file);
        }
        upStatusResponse = await StatusServices.PostStatus(params, formdata);
      } else upStatusResponse = await StatusServices.PostStatus(params, {});

      if (upStatusResponse.status) {
        dispatch(clear_Home());
        dispatch(ReloadHome());
        navigation.goBack();
      } else {
        alert('Server error! Please try again later :(');
      }
    }
  };
  const camera = (mediaType = 'photo') => {
    ImageCropPicker.openCamera({
      width: 500,
      height: 500,
      // cropping: true,
    })
      .then((image) => {
        const arr = [...files];
        const temp = [...imageGrid];
        const value = {
          type: image.mime.includes('image') ? 'image' : 'video',
          uri: image.path,
        };
        arr.push(image);
        temp.push(value);
        setFiles(arr);
        setImageGrid(temp);
      })
      .catch((err) => console.log(err));
  };
  const library = () => {
    ImageCropPicker.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      sortOrder: 'desc',
      // includeExif: true,
      // forceJpg: true,
    })
      .then((images) => {
        setFiles([...files, ...images]);
        const temp = [...imageGrid];
        images.map((image) => {
          const value = {
            type: image.mime.includes('image') ? 'image' : 'video',
            uri: image.path,
          };
          temp.push(value);
        });
        setImageGrid(temp);
      })
      .catch((e) => console.log(e));
  };

  if (Object.keys(userInfo).length !== 0)
    return (
      <>
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            height: '100%',
          }}>
          <ScrollView keyboardShouldPersistTaps="handled">
            <View style={[styles.popupStatus]}>
              <View style={styles.popupStatus}>
                <View
                  style={[
                    styles.popupStatusHeader,
                    {
                      position: 'relative',
                    },
                  ]}>
                  <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={styles.popupStatusHeaderBack}>
                    <Ionicons name="arrow-back" color="black" size={22} />
                  </TouchableOpacity>
                  <View
                    style={[
                      styles.popupStatusHeaderContent,
                      {justifyContent: 'center'},
                    ]}>
                    <Text
                      style={{
                        fontSize: 20,
                      }}>
                      Tạo bài viết
                    </Text>
                  </View>
                  <View
                    style={[
                      styles.popupStatusHeaderButtonSubmit,
                      {justifyContent: 'center'},
                    ]}>
                    <TouchableOpacity
                      style={[
                        styles.submitButton,
                        {
                          backgroundColor:
                            status.length > 0 || files.length > 0
                              ? '#1058B0'
                              : '#EEEEEE',
                        },
                      ]}
                      disabled={isDisabled}
                      onPress={() => postStatus()}>
                      <Text
                        style={{
                          color:
                            status.length > 0 || files.length > 0
                              ? '#f9f3f3'
                              : '#bbbbbb',
                        }}>
                        Đăng
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.divider}></View>

                <View style={styles.divider}></View>
                <View style={[styles.popupStatusContent]}>
                  <View style={[styles.popupStatusUser]}>
                    <View style={styles.avatarBlock}>
                      <Avatar isHomePage={false} source={userInfo.avatar} />
                    </View>
                    <View style={styles.statusUserRestBlock}>
                      <View style={styles.statusUserName}>
                        <Text style={{fontSize: 19}}>{userInfo.userName}</Text>
                      </View>
                      <View style={styles.statusOption}>
                        <DropDownPicker
                          items={[
                            {
                              label: 'Public',
                              value: 'pub',
                              icon: () => (
                                <Ionicons
                                  name="earth-outline"
                                  size={18}
                                  color="#bbbbbb"
                                />
                              ),
                            },
                            {
                              label: 'Private',
                              value: 'priv',
                              icon: () => (
                                <Feather
                                  name="lock"
                                  size={18}
                                  color="#bbbbbb"
                                />
                              ),
                            },
                            {
                              label: 'Friends',
                              value: 'friend',
                              icon: () => (
                                <Ionicons
                                  name="people-outline"
                                  size={18}
                                  color="#bbbbbb"
                                />
                              ),
                            },
                          ]}
                          defaultValue={option}
                          containerStyle={{height: 30, width: 115}}
                          style={styles.statusButton}
                          itemStyle={{
                            justifyContent: 'flex-start',
                          }}
                          dropDownStyle={{backgroundColor: '#fafafa'}}
                          onChangeItem={(item) => setOption(item.value)}
                        />
                      </View>
                    </View>
                  </View>
                  <View style={[styles.popupStatusMainContent]}>
                    <TextInput
                      autoFocus
                      placeholder="What's on your mind?"
                      multiline={true}
                      ref={inputTextRef}
                      style={{
                        textAlign: 'justify',
                        textAlignVertical: 'top',
                        fontSize: 17,
                      }}
                      value={status}
                      onChangeText={setStatus}
                    />
                  </View>
                </View>
                <TouchableOpacity
                  onPress={library}
                  style={{flexDirection: 'row', alignItems: 'center'}}>
                  <FontAwesome
                    name="plus-circle"
                    style={{padding: 3, marginLeft: 10}}
                    size={25}
                    color="blue"
                  />
                  <Text h4 h4Style={{padding: 10}}>
                    Images / Videos
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={camera}
                  style={{flexDirection: 'row', alignItems: 'center'}}>
                  <FontAwesome
                    name="camera"
                    style={{padding: 3, marginLeft: 10}}
                    size={20}
                    color="blue"
                  />

                  <Text h4 h4Style={{padding: 10}}>
                    Camera
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {/* {files.map(fileShowRender)} */}

                <ImageGrid srcImage={imageGrid} />
              </View>
            </View>
          </ScrollView>
        </View>
      </>
    );
  else return <></>;
}
