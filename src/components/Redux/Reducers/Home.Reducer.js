import * as types from '../Constant.ActionType';
var initState = {
  srcData: [],
  err_code: '',
};

var HomeReducer = (state = initState, action) => {
  switch (action.type) {
    case types.ReloadHome_Success:
    case types.Comment_Status_Home: {
      const {data} = action;
      if (data.length === 0) return {...state, err_code: 'No_Things'};
      return {...state, err_code: '', srcData: data};
    }
    case types.Clear_Store_HomePage: {
      return {srcData: [], err_code: ''};
    }

    default:
      return state;
  }
};
export default HomeReducer;
