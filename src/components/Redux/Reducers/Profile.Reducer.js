import AsyncStorage from '@react-native-async-storage/async-storage';
import * as keys from '../Constant.ActionType';
const initState = {
  introUser: {},
  statusUser: [],
  err_code: '',
};

var UserInfo = (state = initState, action) => {
  switch (action.type) {
    case keys.Get_IntroUser_Success: {
      const {data} = action;
      return {...state, introUser: data};
    }
    case keys.GetStatusProfile_Success:
    case keys.Comment_Status_Profile: {
      const {data} = action;
      return {...state, statusUser: data};
    }
    case keys.GetStatusProfile_Failed: {
      const {err} = action;
      if (err === 'No_Status') return {...state, err_code: err};
      // alert(err);
      return {...state, statusUser: [], err_code: err};
    }

    case keys.GetStatusProfile_Failed: {
      const {err} = action;
      return {...state, statusUser: [], err_code: err};
    }
    case keys.Clear_Store_Profile: {
      return {introUser: {}, statusUser: [], err_code: ''};
    }

    case keys.Delete_StatusProfile: {
      const {statusId} = action;
      const temp = [...state.statusUser];
      const index = temp.findIndex((x) => x.id === statusId);
      if (index > -1) {
        temp.splice(index, 1);
      }
      return {...state, statusUser: temp};
    }
    default:
      return state;
  }
};
export default UserInfo;
