import * as types from '../Constant.ActionType';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as storeKeys from '../../Constants';
import API from '../../API/API';
export const Get_IntroUser = () => {
  return async (dispatch) => {
    try {
      const val = await AsyncStorage.getItem(storeKeys.User_Token);

      const params = {
        type_search: '1',
        token: val,
      };
      const route = 'user/search-v1';
      const headers = {
        Authorization: 'bearer' + val,
      };
      const api = new API();

      api
        .onCallAPI('get', route, {}, params, headers)
        .then((res) => {
          if (res.data.error_code !== 0) {
            dispatch({
              type: types.Get_IntroUser_Failed,
              err: res.data.message,
            });
          } else {
            // console.log(res.data.data);
            dispatch({
              type: types.Get_IntroUser_Success,
              data: res.data.data,
            });
            AsyncStorage.setItem(
              storeKeys.User_Avatar,
              res.data.data.user_avatar,
            );
            AsyncStorage.setItem(
              storeKeys.User_Cover,
              res.data.data.user_cover,
            );
            AsyncStorage.setItem(storeKeys.User_Name, res.data.data.user_name);
          }
        })
        .catch((err) => {
          dispatch({
            type: types.Get_IntroUser_Failed,
            err: err,
          });
        });
    } catch (err) {
      dispatch({
        type: types.Get_IntroUser_Failed,
        err: err,
      });
    }
  };
};

export const Clear_Store_Profile = () => {
  return async (dispatch) => {
    try {
      dispatch({type: types.Clear_Store_Profile});
    } catch (err) {
      alert(err);
      dispatch({type: types.Clear_Store_Profile});
    }
  };
};

export const Get_StatusProfile = () => {
  return async (dispatch) => {
    try {
      AsyncStorage.getItem(storeKeys.User_Token).then((val) => {
        if (val) {
          const params = {
            token: val,
          };
          const route = 'status/load-personal-status';

          const headers = {};
          const api = new API();
          api
            .onCallAPI('get', route, {}, params, headers)
            .then((res) => {
              if (res.data.error_code !== 0) {
                alert(res.data.message);
              } else {
                if (res.data.data.length === 0) {
                  dispatch({
                    type: types.GetStatusProfile_Failed,
                    err: 'No_Status',
                  });
                  return;
                }
                const fullStatus = [...res.data.data];
                fullStatus.map(async (x) => {
                  let param = {
                    status_id: x.id,
                  };
                  let route = 'comment';
                  let header = {
                    Authorization: 'bearer' + val,
                  };
                  x.comment = [];

                  let api = new API();
                  api.onCallAPI('get', route, {}, param, header).then((res) => {
                    if (res.data.error_code !== 0) {
                      alert(res.data.message);
                    } else {
                      x.comment = res.data.data;
                    }
                  });
                });
                dispatch({
                  type: types.GetStatusProfile_Success,
                  data: fullStatus,
                  err: '',
                });
              }
            })
            .catch((err) => {
              dispatch({type: types.GetStatusProfile_Failed, err: err});
            });
        } else {
          dispatch({
            type: types.GetStatusProfile_Failed,
            err: 'No Token',
          });
        }
      });
    } catch (err) {
      dispatch({
        type: types.GetStatusProfile_Failed,
        err: err,
      });
    }
  };
};

export const deleteStatus = (statusId) => {
  return async (dispatch) => {
    try {
      const route = 'status/delete';
      const token = await AsyncStorage.getItem(storeKeys.User_Token);
      const param = {
        status_id: statusId,
      };
      const header = {
        Authorization: 'bearer' + token,
      };
      const api = new API();
      api
        .onCallAPI('post', route, {}, param, header)
        .then((res) => {
          if (res.data.error_code !== 0) {
            alert(res.data.message);
          } else {
            dispatch({type: types.Delete_StatusProfile, statusId: statusId});
          }
        })
        .catch((err) => {
          alert('error ', err);
        });
    } catch (err) {
      alert('Have an error when run this feature, the err is: ', err);
    }
  };
};
