import * as types from '../Constant.ActionType';
import API from '../../API/API';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as storeKeys from '../../Constants';

export const ReloadHome = () => {
  return async (dispatch) => {
    try {
      AsyncStorage.getItem(storeKeys.User_Token).then((val) => {
        if (val) {
          var param = {
            token: val,
          };
          var route = 'status/news-feed';
          var header = {
            Authorization: 'bearer' + val,
          };
          var api = new API();
          api
            .onCallAPI('get', route, {}, param, header)
            .then((res) => {
              if (res.data.error_code !== 0) {
                alert(res.data.message);
              } else {
                const fullStatus = [...res.data.data];
                fullStatus.map(async (x) => {
                  let param = {
                    status_id: x.id,
                  };
                  let route = 'comment';
                  let header = {
                    Authorization: 'bearer' + val,
                  };
                  x.comment = [];

                  let api = new API();
                  api.onCallAPI('get', route, {}, param, header).then((res) => {
                    if (res.data.error_code !== 0) {
                      alert(res.data.message);
                    } else {
                      x.comment = res.data.data;
                    }
                  });
                });
                dispatch({
                  type: types.ReloadHome_Success,
                  data: fullStatus,
                  err: '',
                });
              }
            })
            .catch((err) => {
              dispatch({type: types.ReloadHome_Failed, err: err});
              console.log('error: ',err)
            });
        } else {
          dispatch({type: types.ReloadHome_Failed, err: 'No Token'});
        }
      });
    } catch (err) {
      dispatch({type: types.ReloadHome_Failed, err: err});
    }
  };
};

export const clear_Home = () => {
  return async (dispatch) => {
    try {
      dispatch({type: types.Clear_Store_HomePage});
    } catch (err) {
      alert(err);
      dispatch({type: types.Clear_Store_HomePage});
    }
  };
};
