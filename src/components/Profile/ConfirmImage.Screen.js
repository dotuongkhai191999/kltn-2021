import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Avatar, Text, Button} from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useDispatch} from 'react-redux';
import API from '../API/API';
import * as keys from '../Constants';
import {
  Clear_Store_Profile,
  Get_IntroUser,
  Get_StatusProfile,
} from '../Redux/Actions/ProfileUser.Action';

export default function ConfirmImageScreen({navigation, route}) {
  const dispatch = useDispatch();
  const {image} = route.params;
  const imageTemp = {
    uri: image.path,
    width: image.width,
    height: image.height,
    mime: image.mime,
  };
  const {type} = route.params; //true: avatar, false: cover
  const UpImage = async () => {
    const frmdata = new FormData();
    const str = image.path;
    var n = str.lastIndexOf('/');
    var res = str.substr(n + 1);
    const file = {
      uri: image.path,
      name: res,
      type: image.mime,
    };
    frmdata.append('file', file);
    const token = await AsyncStorage.getItem(keys.User_Token);
    const param = {
      update_type: 2,
      type: 0,
      option: type ? 0 : 1, //1 là cover, avartar là 0
    };
    const route = 'user/update/info';
    const headers = {
      Authorization: 'bearer' + token,
    };
    const api = new API();
    api
      .onCallAPI('post', route, frmdata, param, headers)
      .then(async (res) => {
        if (res.data.error_code !== 0) {
          alert(res.data.message);
        } else {
          if (type) await AsyncStorage.setItem(keys.User_Avatar, res.data.data);
          else await AsyncStorage.setItem(keys.User_Cover, res.data.data);
          dispatch(Clear_Store_Profile());
          dispatch(Get_StatusProfile());
          dispatch(Get_IntroUser());
          navigation.goBack();
          // console.log(res.data.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  if (type)
    return (
      <>
        <View
          style={{
            flex: 1,
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginTop: 15,
          }}>
          <Avatar rounded source={imageTemp} size="xlarge" />
          <Text
            h4
            h4Style={{textAlign: 'center', padding: 10, marginVertical: 10}}>
            This is your new avatar, do you want to change it now?
          </Text>
        </View>
        <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
          <Button
            buttonStyle={{width: 150, borderWidth: 2}}
            containerStyle={{margin: 5}}
            icon={<AntDesign name="check" size={15} color="green" />}
            onPress={UpImage}
            title="Confirm"
            type={'outline'}
            titleStyle={{marginHorizontal: 5}}
          />
          <Button
            buttonStyle={{width: 150, borderWidth: 2}}
            containerStyle={{margin: 5}}
            icon={<AntDesign name="close" size={15} color="red" />}
            onPress={() => navigation.goBack()}
            title="Cancel"
            type={'outline'}
            titleStyle={{marginHorizontal: 5}}
          />
        </View>
      </>
    );
  else
    return (
      <>
        <View
          style={{
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginTop: 15,
            flex: 1,
          }}>
          <View style={{height: 200, width: '100%'}}>
            <Image
              source={imageTemp}
              resizeMode="stretch"
              style={{width: '100%', height: '100%'}}
            />
          </View>
          <Text
            h4
            h4Style={{textAlign: 'center', padding: 10, marginVertical: 10}}>
            This is your new cover, do you want to change it now?
          </Text>
        </View>
        <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
          <Button
            buttonStyle={{width: 150, borderWidth: 2}}
            containerStyle={{margin: 5}}
            icon={<AntDesign name="check" size={15} color="green" />}
            onPress={UpImage}
            title="Confirm"
            type={'outline'}
            titleStyle={{marginHorizontal: 5}}
          />
          <Button
            buttonStyle={{width: 150, borderWidth: 2}}
            containerStyle={{margin: 5}}
            icon={<AntDesign name="close" size={15} color="red" />}
            onPress={() => navigation.goBack()}
            title="Cancel"
            type={'outline'}
            titleStyle={{marginHorizontal: 5}}
          />
        </View>
      </>
    );
}

const styles = StyleSheet.create({});
