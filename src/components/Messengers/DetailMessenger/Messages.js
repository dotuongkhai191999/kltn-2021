import React, {useState, useEffect, useRef} from 'react';
import {View, Text, ScrollView, Keyboard, TouchableOpacity} from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';
import Message from './Message';

const Messages = (props) => {
  const [isCopied, setIsCopied] = useState(false);
  // const props.messagesScroll = useRef();
  const copyToClipBoard = (text) => {
    setIsCopied(true);
    Clipboard.setString(text);
  };
  useEffect(() => {
    let t;
    if (isCopied)
      t = setTimeout(() => {
        setIsCopied(false);
      }, 1500);
    return () => {
      clearTimeout(t);
    };
  }, [isCopied]);
  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);
    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => props.setKeyboardStatus(true);
  const _keyboardDidHide = () => props.setKeyboardStatus(false);

  useEffect(() => {
    if (props.messages) {
      if (props.messagesScroll.current) {
        props.messagesScroll.current.scrollToEnd();
      }
    }
  }, [props.messages]);
  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 150;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };
  return (
    <>
      <ScrollView
        onContentSizeChange={() => {
          props.messagesScroll.current.scrollToEnd();
        }}
        onScroll={({nativeEvent}) => {
          if (isCloseToBottom(nativeEvent)) {
            props.setVisibleScroll(false);
          } else {
            props.setVisibleScroll(true);
          }
        }}
        ref={props.messagesScroll}
        style={[props.style, {position: 'relative'}]}>
        <View>
          {props.messages.map((message, i) => {
            return (
              <View key={i}>
                <Message
                  message={message}
                  copyToClipBoard={copyToClipBoard}
                  name={props.selfName}
                  id={props.friend_id}
                />
              </View>
            );
          })}
        </View>
      </ScrollView>
      {isCopied && (
        <View
          style={{
            backgroundColor: 'rgba(0,0,0,.8)',
            position: 'absolute',
            bottom: 60,
            alignSelf: 'center',
            padding: 10,
            borderRadius: 100,
          }}>
          <Text style={{color: 'white', fontSize: 13}}>
            The text is copied to clipboard
          </Text>
        </View>
      )}
    </>
  );
};
export default Messages;
