import React, {useEffect, useRef, useState} from 'react';
import {ActivityIndicator, Text, View} from 'react-native';
import {HeaderBackButton} from '@react-navigation/stack';
import API from '../../API/API';
import HeaderMessenger from './HeaderMessenger';
import Messages from './Messages';
import InputMessage from './Input';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as keys from '../../Constants';
import {SOCKET} from '../../../config';
// import {Keyboard} from 'react-native';

const DetailMessenger = ({route, navigation}) => {
  const {chat_group_id, avatar, friend_chat, friend_id} = route.params;
  const [messages, setMessages] = useState([]);
  const [visibleScroll, setVisibleScroll] = useState(false);
  const messagesScroll = useRef();
  const [room, setRoom] = useState(null);
  const [isDataComming, setisDataComming] = useState(false);
  // const profileLink = await AsyncStorage.getItem(keys.User_ProfLink);
  useEffect(() => {
    const joinRoomChat = async () => {
      const name = await AsyncStorage.getItem(keys.User_ProfLink);
      const room = route.params.chat_group_id;
      if (room) {
        setRoom(room);
        SOCKET.emit('join', {name, room}, (err) => {
          if (err) alert(err);
        });
      }
    };
    joinRoomChat();
  }, [route.params]);

  const [selfName, setSelfName] = useState('');
  useEffect(() => {
    AsyncStorage.getItem(keys.User_ProfLink).then((val) => {
      if (val) setSelfName(val);
    });
  }, []);

  useEffect(() => {
    if (chat_group_id) {
      const route = 'chat/list-messages-in-group';
      const param = {chat_group_id};
      const api = new API();
      api.onCallAPI('get', route, {}, param, {}).then((res) => {
        if (res.data.error_code !== 0) {
          alert(res.data.message);
        } else {
          if (res.data.data) {
            setisDataComming(true);
            const messArr = [];
            res.data.data.map((mess) => {
              const messageText = {
                user: mess.owner,
                text: mess.contents,
                avatar: mess.user_avatar,
              };
              messArr.push(messageText);
              return 0;
            });
            setMessages(messArr);
          }
        }
      });
    }
  }, [chat_group_id]);
  // if (messages.length > 0) console.log(messages);
  const [message, setMessage] = useState('');
  const sendMessage = async () => {
    if (message.trim() === '') return;

    if (message) {
      const token = await AsyncStorage.getItem(keys.User_Token);
      const route = 'chat/save-message';
      const param = {
        chat_group_id,
        content: message.trimEnd(),
      };
      const header = {
        Authorization: 'bearer' + token,
      };
      const api = new API();

      api.onCallAPI('post', route, {}, param, header).then((res) => {
        // console.log('res: ', res.data);
        if (res.data.error_code !== 0) {
          alert(res.data.message);
        }
      });

      const userAvatar = await AsyncStorage.getItem(keys.User_Avatar);
      const username = await AsyncStorage.getItem(keys.User_Name);
      SOCKET.emit(
        'sendMessage',
        message.trimEnd(),
        selfName,
        userAvatar,
        room,
        () => {
          setMessage('');
        },
      );
      //push notification
      // console.log('here ....');

      let notificationData = {
        chat_group_id,
        avatar,
        friend_chat,
        content: `You have a message from ${username}`,
        current_user_avatar: userAvatar,
        moment: 'a few sencond ago', //moment().fromNow(),
        type: 'message',
      };
      // console.log('here')
      SOCKET.emit('client-liked-status', notificationData);
    }
  };

  useEffect(() => {
    SOCKET.on('message', async (message) => {
      const messageText = {
        user: message.user,
        text: message.text,
        avatar: message.avatar,
        time: message.time,
      };
      setMessages((messages) => [...messages, messageText]);
    });
    return () => {
      SOCKET.off('message', console.log('this is clean up messages'));
    };
  }, []);
  const [keyboardStatus, setKeyboardStatus] = useState(false);
  useEffect(() => {
    if (messagesScroll.current) {
      messagesScroll.current.scrollToEnd();
    }
  }, [keyboardStatus]);
  if (!isDataComming) {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignContent: 'center',
          width: 150,
          height: 150,
          zIndex: 999,
          position: 'absolute',
          top: '30%',
          alignSelf: 'center',
          // backgroundColor: 'blue',
        }}>
        <ActivityIndicator size="large" color="black" />
        <Text
          style={{
            textAlign: 'center',
            fontSize: 18,
            fontWeight: 'bold',
            color: 'black',
          }}>
          Waiting
        </Text>
      </View>
    );
  }
  // console.log(props);
  return (
    <View
      style={{
        flex: 1,
        marginBottom: 10,
      }}>
      <HeaderMessenger
        style={{}}
        navigation={navigation}
        avatar={avatar}
        name={friend_chat}
        room={room}
        friend_id={friend_id}
      />
      <Messages
        style={{}}
        keyboardStatus={keyboardStatus}
        setKeyboardStatus={setKeyboardStatus}
        messagesScroll={messagesScroll}
        messages={messages}
        setVisibleScroll={setVisibleScroll}
        selfName={selfName}
        friend_id={friend_id}
      />
      <InputMessage
        style={{}}
        visibleScroll={visibleScroll}
        sendMessage={sendMessage}
        setMessage={setMessage}
        messagesScroll={messagesScroll}
        message={message}
      />
    </View>
  );
};
export default DetailMessenger;
