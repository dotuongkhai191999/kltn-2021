import {useNavigation} from '@react-navigation/native';
import React, {useState, useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import {View, Image, Text} from 'react-native';

const Message = (props) => {
  let isSentByCurrentUser = false;
  const {user} = props.message;
  const navigation = useNavigation();

  //   const trimmedName = 'cá rô';
  const trimmedName = props.name.trim().toLowerCase();
  if (user === trimmedName) {
    isSentByCurrentUser = true;
  }
  //   return <></>;
  return (
    <>
      {isSentByCurrentUser ? (
        <View
          style={{
            padding: 10,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          <View style={{width: '20%'}} />
          <TouchableOpacity
            onLongPress={() => props.copyToClipBoard(props.message.text)}
            style={{
              backgroundColor: 'rgb(110, 223, 0)',
              padding: 10,
              borderRadius: 20,
              marginLeft: 10,
              maxWidth: '80%',
            }}>
            <Text style={{fontSize: 16}}>{props.message.text}</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View
          style={{
            padding: 10,
            width: '80%',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('OtherUser', {
                user_id: props.id,
              })
            }>
            <Image
              style={{width: 40, height: 40, borderRadius: 20}}
              source={{uri: props.message.avatar}}
            />
          </TouchableOpacity>
          {/* avatar của bạn chat */}
          <TouchableOpacity
            onLongPress={() => props.copyToClipBoard(props.message.text)}
            style={{
              backgroundColor: 'rgba(0,0,0,.15)',
              padding: 10,
              borderRadius: 20,
              marginLeft: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 16}}>{props.message.text}</Text>
          </TouchableOpacity>
        </View>
      )}
    </>
  );
};

export default Message;
