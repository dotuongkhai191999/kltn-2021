import {useNavigation} from '@react-navigation/core';
import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Avatar} from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useDispatch} from 'react-redux';
import {Clear_List_Chat, Get_Group_Chat} from '../../Redux/Actions/Chat.Action';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const HeaderMessenger = (props) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const goBackButtonChat = () => {
    dispatch(Get_Group_Chat());
    navigation.navigate('Messengers', {
      screen: 'SmallMessengers',
      resetTime: true,
    });
    // props.navigation.goBack();
  };
  return (
    <View
      style={[
        props.style,
        {
          flexDirection: 'row',
          borderBottomWidth: 0.5,
          borderColor: 'rgba(0,0,0,0.5)',
        },
      ]}>
      <TouchableOpacity
        onPress={goBackButtonChat}
        style={{
          width: 40,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <AntDesign name="arrowleft" size={20} color="rgba(0,0,0,.5)" />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('OtherUser', {
            userId: props.friend_id,
          })
        }
        style={{
          padding: 5,
          flexDirection: 'row',
          paddingLeft: 0,
        }}>
        <Avatar
          containerStyle={{borderWidth: 1.5, borderColor: 'black'}}
          rounded
          size="medium"
          title={props.name}
          source={{uri: props.avatar}}
        />
        <View style={{padding: 5, justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: 'black',
            }}>
            {props.name}
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('VideoCall', {
            action: 'call',
            room: props.room,
            toWho: props.name,
            avatarTo: props.avatar,
            toId: props.friend_id,
          })
        }
        style={{
          alignSelf: 'center',
          position: 'absolute',
          right: 50,
          borderWidth: 1,
          borderRadius: 50,
          padding: 5,
        }}>
        <MaterialIcons name="call" size={25} />
      </TouchableOpacity>
    </View>
  );
};
export default HeaderMessenger;
